using Message_Cipher_API.Interfaces;
using Message_Cipher_API.Models;
using Microsoft.AspNetCore.Mvc;

namespace Message_Cipher_API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EncodeMessageController : ControllerBase
    {
        private readonly IEncoder _encoderService;
        public EncodeMessageController(IEncoder encoderService)
        {
            _encoderService = encoderService;
        }

        [HttpPost(Name = "EnterMessage")]
        public async Task<ActionResult<EnterMessageModel>> CreateEncodeMessage(EnterMessageModel enterMessage)
        {
            return Ok(await _encoderService.EncodingAndSaveMessage(enterMessage));
        }
    }
}