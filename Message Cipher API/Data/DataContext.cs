﻿using Message_Cipher_API.Models;
using Microsoft.EntityFrameworkCore;
using Message_Cipher_API.Data.Configurations;

namespace Message_Cipher_API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { Database.Migrate(); }

        public DbSet<SymbolEncoderModel> EncoderRulesTable { get; set; }
        public DbSet<EnterMessageModel> EnterMessagesTable { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(EnterMessagesTableConfig).Assembly);
        }
    }
}
