﻿using Message_Cipher_API.Models;

namespace Message_Cipher_API.Interfaces
{
    public interface IEncoder
    {
        public Task<EnterMessageModel> EncodingAndSaveMessage(EnterMessageModel enterMessage);
    }
}
