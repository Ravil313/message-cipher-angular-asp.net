﻿using Message_Cipher_API.Data;
using Message_Cipher_API.Interfaces;
using Message_Cipher_API.Models;
using Microsoft.EntityFrameworkCore;

namespace Message_Cipher_API.Services
{
    public class EncoderService : IEncoder
    {
        private readonly DataContext _dataContext;

        public EncoderService(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<EnterMessageModel> EncodingAndSaveMessage(EnterMessageModel enterMessage)
        {
            string encodedMessage = string.Empty;            
            var encodingRules = await _dataContext.EncoderRulesTable
                .AsNoTracking()
                .ToListAsync();

            _dataContext.EnterMessagesTable.Add(enterMessage);
            await _dataContext.SaveChangesAsync();

            for (int i = 0; i < enterMessage.Message?.Length; i++)
            {
                bool isAdded = false;
                foreach (var encodingRule in encodingRules)
                {
                    if (enterMessage.Message[i] == encodingRule.OldSymbol)
                    {
                        encodedMessage += encodingRule.NewSymbol;
                        isAdded = true;
                        break;
                    }
                }
                if (!isAdded)
                    encodedMessage += enterMessage.Message[i];
            }

            var encodedMessageModel = new EnterMessageModel();
            encodedMessageModel.DateTime = enterMessage.DateTime;
            encodedMessageModel.Message = encodedMessage;
            return encodedMessageModel;
        }
    }
}
