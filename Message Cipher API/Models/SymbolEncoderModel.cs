﻿namespace Message_Cipher_API.Models
{
    public class SymbolEncoderModel
    {
        public int Id { get; set; }
        public char OldSymbol { get; set; }
        public char NewSymbol { get; set; }
    }
}
