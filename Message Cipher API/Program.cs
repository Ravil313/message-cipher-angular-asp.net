using Message_Cipher_API.Data;
using Message_Cipher_API.Interfaces;
using Message_Cipher_API.Services;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<DataContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));
builder.Services.AddTransient<IEncoder, EncoderService>();
builder.Services.AddCors(options => options.AddPolicy(name: "EnterMessageOrigns", 
    policy =>
    {
        policy.WithOrigins("http://localhost:4200").AllowAnyMethod().AllowAnyHeader();
    }));
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors("EnterMessageOrigns");

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
