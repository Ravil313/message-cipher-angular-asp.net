﻿namespace Message_Cipher_API.Models
{
    public class EnterMessageModel
    {
        public int Id { get; }
        public string? DateTime { get; set; }
        public string? Message { get; set; }
    }
}
