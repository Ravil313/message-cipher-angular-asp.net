﻿using Message_Cipher_API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Message_Cipher_API.Data.Configurations;

public class EnterMessagesTableConfig : IEntityTypeConfiguration<EnterMessageModel>
{
    public void Configure(EntityTypeBuilder<EnterMessageModel> builder)
    {
        builder.HasKey(enterMessageModel => enterMessageModel.Id);
    }
}