import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EnterMessage } from '../Models/EnterMessage';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EnterMessageService {
  private url = "EncodeMessage";

  constructor(private http: HttpClient) { }

  public postEnterMessage(enterMessage: EnterMessage) : Observable<EnterMessage>{
    return this.http.post<EnterMessage>(`${environment.apiUrl}/${this.url}`, enterMessage);
  }
}
