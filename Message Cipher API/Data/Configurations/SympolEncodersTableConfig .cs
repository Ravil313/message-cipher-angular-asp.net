﻿using Message_Cipher_API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Message_Cipher_API.Data.Configurations;

public class SymbolEncodersTableConfig : IEntityTypeConfiguration<SymbolEncoderModel>
{
    public void Configure(EntityTypeBuilder<SymbolEncoderModel> builder)
    {
        builder.HasKey(symbolEncoderModel => symbolEncoderModel.Id);
    }
}