import { Component, OnInit } from '@angular/core';
import { EnterMessage } from './Models/EnterMessage';
import { EnterMessageService } from 'src/app/Services/enter-message.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Message Cipher UI';
  encodedMessages: EnterMessage[] = [];
  enterMessage = new EnterMessage();


  constructor(private enterMessageService: EnterMessageService){}

  ngOnInit(): void {}

  updateEnterMessage(enterMessage: EnterMessage){
    this.enterMessageService
    .postEnterMessage(enterMessage)
    .subscribe((result: EnterMessage) => (this.encodedMessages.push(result)));
  } 
}
